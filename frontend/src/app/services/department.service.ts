import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Department} from '../models/department';
import {environment} from 'src/environments/environment';


const baseUrl = `${environment.apiUrl}/api/department/`;

@Injectable({
  providedIn: 'root'
})
export class DepartmentService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Department[]> {
    return this.http.get<Department[]>(baseUrl);
  }

  get(id): Observable<Department>  {
    return this.http.get<Department>(`${baseUrl}${id}`);
  }

  create(data): Observable<Department> {
    return this.http.post<Department>(baseUrl, data);
  }

  update(id, data): Observable<Department> {
    return this.http.put<Department>(`${baseUrl}${id}`, data);
  }

  delete(id) {
    return this.http.delete(`${baseUrl}${id}`);
  }

  deleteAll() {
    return this.http.delete(baseUrl);
  }

  findByName(name): Observable<Department[]> {
    return this.http.get<Department[]>(`${baseUrl}/find/${name}`);
  }
}
